<?php

declare(strict_types=1);

namespace JaAdmin\ContactFormModule\Services;

use JaAdmin\CoreModule\Utils\KW;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use JaAdmin\ContactFormModule\Models\ContactFormMessage;
use JaAdmin\CoreModule\Models\EntityManagerDecorator;
use Nette\SmartObject;
use Nette\Utils\ArrayHash;

class ContactFormService
{
    use SmartObject;

    private EntityRepository $repository;

    public function __construct(private EntityManagerDecorator $em)
    {
        $this->repository = $this->em->getRepository(ContactFormMessage::class);
    }

    public function save(ArrayHash $values): ContactFormMessage
    {
        $entity = new ContactFormMessage();
        $entity->setName($values->name)
            ->setEmail($values->email)
            ->setMessage($values->message);

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }

    public function delete(int $id): void
    {
        $item = $this->getItem($id);
        $this->em->remove($item);
        $this->em->flush();
    }

    public function setDone(int $id, bool $done): ContactFormMessage
    {
        $item = $this->getItem($id);
        $item->setDone($done);

        $updatedItem = $item;

        $this->em->persist($item);
        $this->em->flush();

        return $updatedItem;
    }

    public function getQueryBuilder(): QueryBuilder
    {
        return $this->repository->createQueryBuilder(KW::Entity, KW::Entity . KW::Dot . KW::Id);
    }

    public function getItem(int $id): ?ContactFormMessage
    {
        return $this->repository->findOneBy([KW::Id => $id]);
    }
}
