<?php

declare(strict_types=1);

namespace JaAdmin\ContactFormModule\Presenters;

final class DetailPresenter extends BasePresenter
{
    public function renderDefault(int $id)
    {
        $this->template->item = $this->contactFormService->getItem($id);
    }
}
