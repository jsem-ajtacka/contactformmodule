<?php

declare(strict_types=1);

namespace JaAdmin\ContactFormModule\Presenters;

use JaAdmin\CoreModule\Utils\FlashMessage;
use JaAdmin\CoreModule\Utils\FlashMessageType;
use JaAdmin\CoreModule\Utils\Privilege;
use JaAdmin\CoreModule\Authorization\Resource;

final class OverviewPresenter extends BasePresenter
{
    private const RedirectLink = ":ContactForm:Overview:default";
    private const DeleteSuccess = "contactFormModule.overview.flashMessage.deleteSuccess";

    public function renderDefault()
    {
        $isUserAllowedToDelete = $this->getUser()->isAllowed(self::ExtensionName, Privilege::Delete);
        $this->template->isUserAllowedToDelete = $isUserAllowedToDelete;

        if (!isset($this->template->items)) {
            $this->template->items = $this->getItems();
        }
    }

    public function handleDelete(int $id)
    {
        $this->contactFormService->delete($id);
        $this->flashMessage(new FlashMessage(self::DeleteSuccess, FlashMessageType::Success));
        $this->redirect(self::RedirectLink);
    }

    public function handleDone(int $id, bool $done)
    {
        $updatedItem = $this->contactFormService->setDone($id, $done);

        $this->template->items = $this->isAjax() ? [] : $this->getItems();
        $this->template->items[$id] = $updatedItem;
        $this->redrawControl("rows");
    }

    private function getItems(): array
    {
        return $this->contactFormService->getQueryBuilder()->getQuery()->getResult();
    }
}
