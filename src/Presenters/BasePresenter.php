<?php

declare(strict_types=1);

namespace JaAdmin\ContactFormModule\Presenters;

use JaAdmin\ContactFormModule\Services\ContactFormService;
use JaAdmin\CoreModule\Presenters\SecurePresenter;
use Nette\DI\Attributes\Inject;

class BasePresenter extends SecurePresenter
{
    protected const ExtensionName = "contactForm";
    private const RedirectLink = ":Core:Overview:default";

    #[Inject]
    public ContactFormService $contactFormService;

    public function startup()
    {
        parent::startup();

        if (!$this->extensionService->getItem(self::ExtensionName)->isActive()) {
            $this->redirect(self::RedirectLink);
        }
    }
}
