<?php

declare(strict_types=1);

namespace JaAdmin\ContactFormModule\DI;

use Nette\DI\CompilerExtension;

final class ContactFormModuleExtension extends CompilerExtension {}