<?php

declare(strict_types=1);

namespace JaAdmin\ContactFormModule\Models;

use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use JaAdmin\CoreModule\Models\BaseEntity;

#[ORM\Entity(repositoryClass: EntityRepository::class)]
#[ORM\Table(name: "contact_form_message")]
class ContactFormMessage extends BaseEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private int $id;

    #[ORM\Column(type: "string")]
    private string $name;

    #[ORM\Column(type: "string")]
    private string $email;

    #[ORM\Column(type: "string")]
    private string $message;

    #[ORM\Column(type: "boolean")]
    private bool $done;

    #[ORM\Column(type: "datetime")]
    protected DateTime $sentAt;

    public function __construct()
    {
        $this->done = false;
        $this->sentAt = new DateTime();
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function isDone(): bool
    {
        return $this->done;
    }

    public function setDone(bool $done): void
    {
        $this->done = $done;
    }

    public function getSentAt(): DateTime
    {
        return $this->sentAt;
    }

    public function setSentAt(DateTime $sentAt): void
    {
        $this->sentAt = $sentAt;
    }
}
